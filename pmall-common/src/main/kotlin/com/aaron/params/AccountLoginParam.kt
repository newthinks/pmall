package com.aaron.params

data class AccountLoginParam(val username:String, val password:String)