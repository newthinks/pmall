package com.aaron.dao;

import com.aaron.models.PodAccountRoleRelation;
import com.aaron.models.PodAccountRoleRelationExample;

public interface PodAccountRoleRelationMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_account_role_relation
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    long countByExample(PodAccountRoleRelationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_account_role_relation
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int deleteByPrimaryKey(Integer ar_id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_account_role_relation
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int insert(PodAccountRoleRelation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_account_role_relation
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int insertSelective(PodAccountRoleRelation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_account_role_relation
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    PodAccountRoleRelation selectByPrimaryKey(Integer ar_id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_account_role_relation
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int updateByPrimaryKeySelective(PodAccountRoleRelation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_account_role_relation
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int updateByPrimaryKey(PodAccountRoleRelation record);
}