package com.aaron.dao;

import com.aaron.models.PodRole;
import com.aaron.models.PodRoleExample;

public interface PodRoleMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_role
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    long countByExample(PodRoleExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_role
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int deleteByPrimaryKey(Integer r_id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_role
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int insert(PodRole record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.pod_role
     *
     * @mbg.generated Sun Sep 29 16:58:40 CST 2019
     */
    int insertSelective(PodRole record);
}