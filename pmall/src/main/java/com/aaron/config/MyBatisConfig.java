package com.aaron.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.aaron.dao","com.aaron.mapper"})
public class MyBatisConfig {

}
