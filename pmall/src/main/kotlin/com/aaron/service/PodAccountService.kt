package com.aaron.service

import com.aaron.models.PodAccount
import com.aaron.params.AccountLoginParam
import com.aaron.params.AccountRegisterParam


interface PodAccountService {
	fun login(loginParam: AccountLoginParam):PodAccount?
	
	fun register(registerParam: AccountRegisterParam): PodAccount?
}