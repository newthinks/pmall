package com.aaron

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class PMallApplication

fun main(args: Array<String>){
	SpringApplication.run(PMallApplication::class.java)
}