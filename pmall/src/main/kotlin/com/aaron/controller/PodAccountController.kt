package com.aaron.controller

import com.aaron.models.PodAccount
import com.aaron.params.AccountLoginParam
import com.aaron.params.AccountRegisterParam
import com.aaron.service.PodAccountService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.bind.BindResult
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
@RequestMapping("/account")
@Api(tags = ["用户控制器"],description = "提供用户操作接口")
class PodAccountController {
	@Autowired
	lateinit var accountService: PodAccountService
	
	
	@ApiOperation("/login",tags=["用户登录"])
	@GetMapping("/login")
	fun login():ModelAndView{
		val mv:ModelAndView= ModelAndView();
		mv.viewName="account/login"
		return mv
	}
	
	@PostMapping("/login")
	fun login(@ModelAttribute param:AccountLoginParam,result:BindingResult,request:HttpServletRequest,response:HttpServletResponse):ModelAndView{
		val mv= ModelAndView()
		val account = accountService.login(param);
		if(account!=null){
			request.session.setAttribute("account",account)
			mv.addObject("hasSuccess","登陆成功!")
			mv.viewName="home/index"
		}else{
			mv.addObject("hasError","登陆失败!")
			mv.viewName="account/login"
		}
		return mv
	}
	
	@GetMapping("/register")
	fun register():String{
		return "account/register";
	}
	
	@PostMapping("/register")
	fun register(@ModelAttribute params:AccountRegisterParam,result:BindingResult,request:HttpServletRequest):ModelAndView{
		val mv= ModelAndView()
		return mv;
	}
	
}